<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 12/05/2019
 * Time: 16:38
 */
namespace App\Model\Cards;

class NewUsersCard extends Card
{
    private $yesterdayCard;
    private $weekCard;
    private $monthCard;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->metrics = ['ga:newUsers'];
        $this->dimensions = ['ga:date'];
    }

    public function buildBeforeYesterdayCar(array $arrayReport) : bool
    {
        if(count($arrayReport) <= 1) //s'il n'y a qu'un ou zéro jour de données
        {
            return false;
        }
        $beforeYesterday = new YesterdayCard();
        $beforeYesterday->setTitle('avant hier');
        $beforeYesterday->setDate($arrayReport[count($arrayReport)-2]['date']);
        $beforeYesterday->setVisitValue($arrayReport[count($arrayReport)-2]['nb_visits']);
        $this->yesterdayCard->setBeforeYesterdayCard($beforeYesterday);
        return true;
    }

    public function buildYesterdayCard(array $arrayReport) : bool
    {
        if(empty($arrayReport))
        {
            return false;
        }
        $yc = new YesterdayCard();
        $yc->setTitle('hier');
        $yc->setDate(end($arrayReport)['date']);
        $yc->setVisitValue(end($arrayReport)['nb_visits']);
        $this->yesterdayCard = $yc;
        $this->buildBeforeYesterdayCar($arrayReport);
        return true;
    }

    public function buildArrayFromReports($reports) : void
    {
        $newUsersByDay = ['new_users'];

        /*Affichage des resulats*/
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();
            $newUsersByDayTemp = [];
            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders ? $dimensionHeaders : []) && $i < count($dimensions ? $dimensions : []); $i++) {
                    //echo($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
                    //echo "<br>";
                    $newUsersByDayTemp['date'] = \DateTime::createFromFormat('Ymd', $dimensions[$i]);
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        //echo($entry->getName() . ": " . $values[$k] . "\n");
                        //echo "<br>";
                        $newUsersByDayTemp['nb_visits'] = $values[$k];
                    }
                }
                $newUsersByDay[] = $newUsersByDayTemp;
            }
            //echo "<br><br><br>";
        }
        $this->arrayFromReport = $newUsersByDay;
    }

    /**
     * @return mixed
     */
    public function getYesterdayCard()
    {
        return $this->yesterdayCard;
    }

    public function getBeforeYesterdayCard()
    {
        return $this->getYesterdayCard()->getBeforeYesterdayCard();
    }

}