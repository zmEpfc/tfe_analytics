<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 12/05/2019
 * Time: 17:12
 */

namespace App\Model\Cards;


class DeviceTypeCard extends Card
{
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->dimensions = ['ga:deviceCategory'];
        $this->setOrdering = true;
    }
}