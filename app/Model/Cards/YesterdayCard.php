<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 19/05/2019
 * Time: 12:32
 */

namespace App\Model\Cards;


class YesterdayCard
{
    private $title;
    private $date;
    private $visitValue;
    //Servira à comparer avec avant-hier par exemple
    private $beforeYesterdayCard;
    private $diffVisit; //Difference between yesterday and beforeYesterday in percent
    private $materialIconClass;


    public function compareBeforeYesterday(YesterdayCard $beforeYesterdayCard) : int
    {
        if(empty($beforeYesterdayCard))
        {
            return 0;
        }
        $yesterdayVisits = $this->getVisitValue();
        $beforeYesterdayVisits = $beforeYesterdayCard->getVisitValue();
        $this->materialIconClass = $this->buildMaterialIconClass($beforeYesterdayVisits);
        return $this->computePercentage($beforeYesterdayVisits, $yesterdayVisits);
    }

    private function computePercentage(int $oldNb, int $newNb) : int
    {
        if($oldNb === 0 || (!$oldNb && $newNb) || ($oldNb === 0 && $newNb > 0))
        {
            return 100 * $newNb;
        }
        else{
            $diff = $newNb - $oldNb;
            $percentChange = ($diff/$oldNb)*100;
            return number_format($percentChange, 2);
        }
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * build a datetime which represent yesterday related to given $date
     * and assign it
     * @param $date
     * @throws \Exception
     */
    public function setDate($date): void
    {
        $date->sub(new \DateInterval('P1D'));
        $this->date = $date;
    }

    /**
     * @return null
     */
    public function getVisitValue()
    {
        return $this->visitValue;
    }

    /**
     * @param null $visitValue
     */
    public function setVisitValue($visitValue): void
    {
        $this->visitValue = $visitValue;
    }

    /**
     * @return mixed
     */
    public function getBeforeYesterdayCard()
    {
        return $this->beforeYesterdayCard;
    }

    /**
     * @param mixed beforeYesterdayCard
     */
    public function setBeforeYesterdayCard(YesterdayCard $beforeYesterdayCard)
    {
        $this->diffVisit = $this->compareBeforeYesterday($beforeYesterdayCard);
        $this->beforeYesterdayCard = $beforeYesterdayCard;
    }

    /**
     * @return mixed
     */
    public function getDiffVisit()
    {
        return $this->diffVisit;
    }

    /**
     * @param int $diffVisit : diff of visitors between yesterday and before yesterday
     * @return string
     */
    public function buildMaterialIconClass(int $diffVisit) : string
    {
        $res = 'blue-text';
        if($diffVisit > 0)
        {
            $res = 'green-text';
        } elseif($diffVisit < 0){
            $res = 'red-text';
        }
        return $res;
    }

    /**
     * @return mixed
     */
    public function getMaterialIconClass()
    {
        return $this->materialIconClass;
    }

}