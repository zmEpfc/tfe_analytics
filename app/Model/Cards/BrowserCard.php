<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 12/05/2019
 * Time: 19:35
 */

namespace App\Model\Cards;


class BrowserCard extends Card
{
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->dimensions = ['ga:browser'];
        $this->setOrdering = true;
    }

    public function buildArrayFromReports($reports)
    {
        /*Affichage des resulats*/
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();
            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders ? $dimensionHeaders : []) && $i < count($dimensions ? $dimensions : []); $i++) {
                    echo($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
                    echo "<br>";
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        echo($entry->getName() . ": " . $values[$k] . "\n");
                        echo "<br>";
                    }
                }
            }
            echo "<br><br><br>";
        }
    }
}