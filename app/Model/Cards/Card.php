<?php
namespace App\Model\Cards;

use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_OrderBy;
use Google_Service_AnalyticsReporting_ReportRequest;
use Illuminate\Database\Eloquent\Model;


abstract class Card extends model
{
    private $client;
    private $dateRange;
    private $viewId;
    protected $metrics;
    protected $dimensions;
    protected $setOrdering;
    protected $arrayFromReport;


    public function __construct(array $config)
    {
        $this->client = $config['client'];
        $this->viewId = $config['viewId'];
        $this->dateRange = $this->buildDateRange($config['startDate'], $config['endDate']);
    }

    public function buildRequest()
    {
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($this->viewId);
        $request->setDateRanges($this->dateRange);
        $request->setIncludeEmptyRows(true);

        $request = $this->buildMetricProcess($request); // bind the metric query
        $request = $this->buildDimension($request); // bind the dimension query
        $request = $this->buildOrdering($request);
        return $request;
    }

    private function buildOrdering($request)
    {
        if(!$request || $this->setOrdering === false)
        {
            return $request;
        }
        //$ordering = $this->buildOrderingProcess("ga:visits", "VALUE", "DESCENDING");
        //$request->setOrderBys($ordering);
        return $request;
    }

    private function buildDimension($request)
    {
        if(empty($this->dimensions))
        {
           return $request;
        }
        $dimensionsForRequest = [];
        foreach($this->dimensions as $dimension)
        {
            $dimensionObject = new Google_Service_AnalyticsReporting_Dimension();
            $dimensionObject->setName($dimension);
            $dimensionsForRequest[] =  $dimensionObject;
        }
        $request->setDimensions($dimensionsForRequest);
        return $request;
    }

    private function buildMetricProcess($request)
    {
        if(empty($this->metrics))
        {
            return $request;
        }
        $metricsForRequest = [];
        $i = 1;
        foreach($this->metrics as $metric)
        {
            $metricObject = new Google_Service_AnalyticsReporting_Metric();
            $metricObject->setExpression($metric);
            //$metricObject->setAlias('metric_' . $i);
            $metricsForRequest[] = $metricObject;
            ++$i;
        }
        $request->setMetrics($metricsForRequest);
        return $request;
    }

    private function buildOrderingProcess(string $fieldName, string $value, string $order)
    {
        $ordering = new Google_Service_AnalyticsReporting_OrderBy();
        $ordering->setFieldName($fieldName);
        $ordering->setOrderType($value);
        $ordering->setSortOrder($order);
        return $ordering;
    }

    private function buildDateRange(string $startDate, string $endDate)
    {
        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDate);
        $dateRange->setEndDate($endDate);
        return $dateRange;
    }

    /**
     * @return mixed
     */
    public function getArrayFromReport()
    {
        return $this->arrayFromReport;
    }



}