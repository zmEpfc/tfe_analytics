<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Faker\Provider\DateTime;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_Oauth2;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    //redirect after logout
    protected $redirectAfterLogout = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    private function buildClientConfig()
    {
        $client = new Google_Client();
        $client->setClientId("150866524909-kj7sb5c0f9i99240n89a2rqdfarp98o3.apps.googleusercontent.com");
        $client->setClientSecret("y0N_2oEgPTut8kHkuYDfs5gz");
        $client->setRedirectUri("http://localhost:8000/login/google/callback");
        $client->setScopes("https://www.googleapis.com/auth/analytics.readonly");
        $client->addScope("email");
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('auto');
        return $client;
    }

    private function clientFetchAccessToken(Request $request)
    {
        $client = $this->buildClientConfig();
        $authCode = $request->query('code');
        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);
        //enregistre le credential en session pour pouvoir faire des requètes
        session(['access_token' => $accessToken]);
        return $client;
    }

    private function getEmailFromGoogle($client)
    {
        $oauth2 = new Google_Service_Oauth2($client);
        return $oauth2->userinfo->get()->email;
    }

    private function createUserIfNotExists(string $email)
    {
        $user = User::where('email', $email)->first();
        if (!$user) {
            $user = new User();
            $user->name = $email;
            $user->email = $email;
            $user->password = \Hash::make($email);
            if($user->save())
            {
                auth()->login($user);
            }
        } else
        {
            auth()->login($user);
        }
    }

    public function googleCallback(Request $request)
    {
        if ($request->has('code')) {
            //1. Configure le client
            $client = $this->clientFetchAccessToken($request);

            //2. Récupère email du user s'est connecté via Google signIn
            $email = $this->getEmailFromGoogle($client);

            //3. si cet email n'existe pas en bdd on enregistre un nouveau user
            $this->createUserIfNotExists($email);

            //4. Redirige vers DashboardController
            return redirect()->route('selection');

        } else {
            return redirect()->route('dashboard');
        }
    }

    public function logout(Request $request)
    {
        Session::flush();
        Auth::logout();
        return redirect()->route('/');
    }
}
