<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 22/04/2019
 * Time: 20:39
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Google_Client;
use Google_Service_Analytics;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function view()
    {
        $client = new Google_Client();
        $client->setClientId("150866524909-kj7sb5c0f9i99240n89a2rqdfarp98o3.apps.googleusercontent.com");
        $client->setClientSecret("y0N_2oEgPTut8kHkuYDfs5gz");
        $client->setRedirectUri("http://localhost:8000/login/google/callback");
        $client->setScopes("https://www.googleapis.com/auth/analytics.readonly");
        $client->addScope("email");
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('auto');
//        if (Storage::disk('local')->exists('tokenSaved.json'))
//        {
//            $jsonFile = Storage::get('tokenSaved.json');
//            $accessToken = json_decode($jsonFile, true);
//            $client->setAccessToken($accessToken);
//
//            $Google_Service_Analytics = new Google_Service_Analytics($client);
//            foreach ($Google_Service_Analytics->management_accountSummaries->listManagementAccountSummaries()->items as $view) {
//                echo $view->name . PHP_EOL;
//            }
//
//
//            dd('ready for request in dashboard controller');
//            //ok ready to make a request
//        } else{
        // Create the url link
        $auth_url = $client->createAuthUrl();
        return view('welcome', [
            'auth_url' => $auth_url
        ]);
        //}
    }
}