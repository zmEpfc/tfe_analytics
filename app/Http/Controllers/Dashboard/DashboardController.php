<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 22/04/2019
 * Time: 12:42
 */

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Cards\BrowserCard;
use App\Model\Cards\Card;
use App\Model\Cards\CountryAndCityCard;
use App\Model\Cards\DeviceTypeCard;
use App\Model\Cards\NewUsersCard;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_OrderBy;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_Oauth2;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class DashboardController extends Controller
{
    public function view()
    {
        $startDate = "777daysAgo";
        $endDate = "today";
        $viewId = session('user-property-selection');
        $client = $this->buildClientConfig();
        $configParams = ['client' => $client, 'viewId' => $viewId, 'startDate' => $startDate, 'endDate' => $endDate];

        /* Préparation des 5 premières requètes */
        $newUsersCard = new NewUsersCard($configParams);
        $newUsersCardRequest = $newUsersCard->buildRequest();

//        $deviceTypeCard = new DeviceTypeCard($configParams);
//        $deviceTypeCardRequest = $deviceTypeCard->buildRequest();
//
//        $countryAndCityCard = new CountryAndCityCard($configParams);
//        $countryAndCityCardRequest = $countryAndCityCard->buildRequest();
//
//        $browserCard = new BrowserCard($configParams);
//        $BrowserCardRequest = $browserCard->buildRequest();

        //List des requêtes : un maximum de 5 requêtes est autorisé par Google
        $requests = [$newUsersCardRequest];

        $reports = $this->execRequest($client, $requests);

        $newUsersCard->buildArrayFromReports($reports);
        $newUsersCard->buildYesterdayCard($newUsersCard->getArrayFromReport());

        return view("dashboard.dashboard", [
            'yesterdayCard' => $newUsersCard->getYesterdayCard(),
            'beforeYesterdayCard' => $newUsersCard->getBeforeYesterdayCard(),
            'newUsersReport' => $newUsersCard->getArrayFromReport()
        ]);
    }

    private function execRequest($client, array $requests)
    {
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests($requests);
        $analytics = new Google_Service_AnalyticsReporting($client);
        return $reports = $analytics->reports->batchGet($body);
    }

    private function buildClientConfig()
    {
        $client = new Google_Client();
        $client->setClientId("150866524909-kj7sb5c0f9i99240n89a2rqdfarp98o3.apps.googleusercontent.com");
        $client->setClientSecret("y0N_2oEgPTut8kHkuYDfs5gz");
        $client->setRedirectUri("http://localhost:8000/login/google/callback");
        $client->setScopes("https://www.googleapis.com/auth/analytics.readonly");
        $client->addScope("email");
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('auto');
        $client->setAccessToken(session('access_token'));
        $client->fetchAccessTokenWithAuthCode(session('access_token')['access_token']);
        return $client;
    }

    private function buildQuerySources($viewProperty, $dateRange)
    {
        // Create the Ordering.
        $ordering = $this->buildOrdering("ga:visits", "VALUE", "DESCENDING");

        // Creating the dimension
        $pageViews = new Google_Service_AnalyticsReporting_Dimension();
        $pageViews->setName("ga:source");

        $request5 = new Google_Service_AnalyticsReporting_ReportRequest();
        $request5->setViewId($viewProperty);
        $request5->setDateRanges($dateRange);
        $request5->setDimensions(array($pageViews));
        $request5->setOrderBys($ordering);
        return $request5;
    }

    private function buildQueryPageViews($viewProperty, $dateRange)
    {
        // Create the Ordering.
        $ordering = $this->buildOrdering("ga:visits", "VALUE", "DESCENDING");

        // Creating the dimension
        $pageViews = new Google_Service_AnalyticsReporting_Dimension();
        $pageViews->setName("ga:pageTitle");

        $request4 = new Google_Service_AnalyticsReporting_ReportRequest();
        $request4->setViewId($viewProperty);
        $request4->setDateRanges($dateRange);
        $request4->setDimensions(array($pageViews));
        $request4->setOrderBys($ordering);
        return $request4;
    }

    private function buildQueryBrowser($viewProperty, $dateRange)
    {
        // Create the Ordering
        $ordering = $this->buildOrdering("ga:visits", "VALUE", "DESCENDING");
        // Creating the dimension
        $browser = new Google_Service_AnalyticsReporting_Dimension();
        $browser->setName("ga:browser");
        $request2 = new Google_Service_AnalyticsReporting_ReportRequest();
        $request2->setViewId($viewProperty);
        $request2->setDateRanges($dateRange);

        $request2->setDimensions(array($browser));
        $request2->setOrderBys($ordering);
        return $request2;
    }

    private function buildOrdering(string $fieldName, string $value, string $order)
    {
        $ordering = new Google_Service_AnalyticsReporting_OrderBy();
        $ordering->setFieldName($fieldName);
        $ordering->setOrderType($value);
        $ordering->setSortOrder($order);
        return $ordering;
    }

    private function printResult($reports){
        /*Affichage des resulats*/
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();
            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders ? $dimensionHeaders : []) && $i < count($dimensions ? $dimensions : []); $i++) {
                    print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        print($entry->getName() . ": " . $values[$k] . "\n");
                    }
                }
            }
        }
    }
}