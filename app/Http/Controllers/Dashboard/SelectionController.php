<?php
/**
 * Created by PhpStorm.
 * User: zmmai
 * Date: 01/05/2019
 * Time: 08:41
 */

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_Oauth2;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SelectionController extends Controller
{
    public function view()
    {
        echo 'on est dans selection';
        $client = new Google_Client();
        $client->setClientId("150866524909-kj7sb5c0f9i99240n89a2rqdfarp98o3.apps.googleusercontent.com");
        $client->setClientSecret("y0N_2oEgPTut8kHkuYDfs5gz");
        $client->setRedirectUri("http://localhost:8000/login/google/callback");
        $client->setScopes("https://www.googleapis.com/auth/analytics.readonly");
        $client->addScope("email");
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('auto');
        $client->setAccessToken(session('access_token'));

        $Google_Service_Analytics = new Google_Service_Analytics($client);
        $websites = $Google_Service_Analytics->management_accountSummaries->listManagementAccountSummaries()->items;

        if(count($websites) < 1){
            echo "Vous devez avoir au moins 1 site internet dans google analytics";
            return redirect()->route('dashboard');
        }

        $userProperties = []; //les sites web de cet utilisateur
        foreach ($websites as $key => $view) {
            $userProperties[$key] = [];
            $websiteName = $view->name;
            $websiteUrl = $view->webProperties[0]->websiteUrl;
            $propertyId = $view->webProperties[0]->profiles[0]->id;
            $userProperties[$key]['name'] = $websiteName;
            $userProperties[$key]['url'] = $websiteUrl;
            $userProperties[$key]['id'] = $propertyId;
        }
        return view("dashboard.selection")->with([
            'userProperties' => $userProperties
        ]);
    }

    public function handleSelectionSubmit(Request $request)
    {
        $propertySelected = $request->input('user-property-selection');
        session(['user-property-selection' => $propertySelected]);
        return redirect()->route('dashboard');
    }
}