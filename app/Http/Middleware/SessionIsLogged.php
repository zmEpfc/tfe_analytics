<?php

namespace App\Http\Middleware;

use Closure;

class SessionIsLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('logged_oauth2') === true)
        {
            if(!empty(session('credientials')))
            {
                return $next($request);
            }
        }
        session(['logged_oauth2' => false]);
        return redirect()->route('home');
    }
}
