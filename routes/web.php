<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HOME
Route::get('/', 'Home\HomeController@view')->name('/');
Route::get('/home', 'Home\HomeController@view')->name('home');

// AUTH
Route::get('/login/google/callback', 'Auth\LoginController@googleCallback');
Route::get('/logout', 'Auth\LoginController@logout')
    ->middleware('auth')
    ->name('logout');

// DASHBOARDS
Route::get('/dashboard', 'Dashboard\DashboardController@view')
    ->middleware('auth')
    ->name('dashboard');

// SELECTION
Route::get('/selection', 'Dashboard\SelectionController@view')
    ->middleware('auth')
    ->name('selection');

Route::post('/selectionSubmitting', 'Dashboard\SelectionController@handleSelectionSubmit')
    ->middleware('auth')
    ->name('selectionSubmitting');

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes(['register' => false]);

Route::get('/login', 'Home\HomeController@view')->name('login');



