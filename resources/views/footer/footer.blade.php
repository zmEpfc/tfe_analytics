<footer class="page-footer footer footer-static cyan navbar-border navbar-shadow">
    <div class="container">
        <div class="row section">
            <div class="col l6 s12">
                <h5 class="white-text">World Market</h5>
                <p class="grey-text text-lighten-4">World map, world regions, countries and cities.</p>
                <div id="world-map-markers"></div>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Sales by Country</h5>
                <p class="grey-text text-lighten-4">A sample polar chart to show sales by country.</p>
                <div id="polar-chart-holder"></div>
                <canvas id="polar-chart" width="200"></canvas>
            </div>
        </div>
    </div>
    <div class="page-footer pt-0 footer-dark gradient-45deg-light-blue-cyan gradient-shadow">
        <div class="footer-copyright">
            <div class="container"><span></span>&copy; 2019          <a href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a> All rights reserved.<span class="right hide-on-small-only"></span>Design and Developed by <a href="https://pixinvent.com/">PIXINVENT</a></div>
        </div>
    </div>
</footer>