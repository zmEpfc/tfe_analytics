@extends('layouts.app')

@section('css_in_head')
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/jquery-jvectormap/jquery-jvectormap.css') }}">

    <!-- END VENDOR CSS-->
    <!-- BEGIN THEME  CSS-->
    <!-- END THEME  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/horizontal-menu-template/materialize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/horizontal-menu-template/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/layouts/style-horizontal.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pages/dashboard.css')}}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
@endsection

@section('content')
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns  " data-open="click" data-menu="horizontal-menu" data-col="2-columns">
{{--    @include('header.header')--}}
    {{--@include('header.sidenav')--}}
    @include('body.body')
{{--
    @include('footer.footer')
--}}
<p><a href="{{ route('logout') }}">Logout</a></p>
    </body>
@endsection

@section('js_bottom')
    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="{{ asset('js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('vendors/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('vendors/chartjs/chart.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('vendors/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="{{ asset('js/plugins.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/custom/custom-script.js') }}" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('js/scripts/dashboard-analytics.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/scripts/vectormap-script.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/scripts/dashboard-ecommerce.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
@endsection