<p>Ici l'utilisateur choisit la propriété</p>
<form action="{{\Illuminate\Support\Facades\URL::to('/selectionSubmitting')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <select name="user-property-selection">
        @foreach($userProperties as $key => $userProp)
                <option value="{{$userProp['id']}}" data-url="{{$userProp['url']}}">
                    {{$userProp['name']}}
                </option>
            @endforeach
        </select>
        <button type="submit">Envoyer</button>
</form>