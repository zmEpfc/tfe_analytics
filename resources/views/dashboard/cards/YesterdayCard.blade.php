<div class="col s12 m4 l4">
    <div id="weekly-earning" class="card animate fadeUp">
        <div class="card-content">
            <div class="input-field col s6">
            <h4 class="header m-0">Hière
                <span data-tooltip="Par rapport à avant-hière" data-position="top" class="tooltipped {{$yesterdayCard->getMaterialIconClass()}} small text-darken-1 ml-1">
                    <i class="material-icons" style="position:relative; top:5px;">
                    @if($yesterdayCard->getDiffVisit() >= 0)
                        {{'keyboard_arrow_up'}}
                    @else
                        {{'keyboard_arrow_down'}}
                    @endif
                        </i>
                    @if(is_numeric($yesterdayCard->getDiffVisit()))
                        {{$yesterdayCard->getDiffVisit()}}%
                    @endif
                </span>
            </h4>
                <span class="no-margin grey-text lighten-3 medium-small">
                    {{ \Carbon\Carbon::parse($yesterdayCard->getDate())->format('D d M Y')}}
                </span>
            </div>
            <div class="input-field col s6">
                <!-- Dropdown Trigger -->
                <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>Période</a>
                <!-- Dropdown Structure -->
                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="#!">Hière</a></li>
                    <li><a href="#!">Semaine dernière</a></li>
                    <li><a href="#!">Mois dernier</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">three</a></li>
                    <li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
                    <li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
                </ul>
            </div>

        {{--<canvas id="monthlyEarning" class="" height="150"></canvas>--}}
            <div class="input-field col s12 center-align">
                <span style="font-size: 120px;">
                    {{$yesterdayCard->getVisitValue()}}
                    {{--<i class="material-icons deep-orange-text text-accent-2">arrow_upward</i>--}}
                </span>
                <h3 class="header mt-0">Visiteurs</h3>
                <a class="waves-effect waves-light btn gradient-45deg-purple-deep-orange gradient-shadow">
                    INFORMATIONS
                </a>
            </div>
        </div>
    </div>
</div>